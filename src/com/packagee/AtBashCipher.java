package com.packagee;

public class AtBashCipher implements Cipher {
    @Override
    public String decode(String message) {
        return encode(message);
    }
    @Override
    public String encode(String message) {
        StringBuilder encodedMessage = new StringBuilder();
        for(char toEncode: message.toCharArray()){
            if(toEncode==' '){
                encodedMessage.append(toEncode);
            }
            else if(toEncode>='a' && toEncode<='z'){
                char encoded=(char)(toEncode+(25-2*(toEncode-97))); //A - 97, Z - 122
                encodedMessage.append(encoded);
            }
            else if(toEncode>='A' && toEncode<='Z'){
                char encoded=(char)(toEncode+(25-2*(toEncode-65)));//a - 65, z - 90
                encodedMessage.append(encoded);
            }
        }
        return encodedMessage.toString();
    }
}
